# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 2019-09-16

### Changed

- Upgrade gradle to v5.4.1

## 2019-03-01

### Changed

- Make code more DRY by using `m4` to generate `Dockerfile`s from common snippets.
- Upgrade all system packages.
- Upgrade gradle to v4.10.3.

## 2019-01-05

### Changed

- Fresh rebuild to get latest system updates and renew latest android sdk licences.

## 2018-10-12

### New

- Initial release
- Use Android SDK v28 and Gradle v4.6