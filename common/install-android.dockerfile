#
# Install Android SDK
#

ENV ANDROID_SDK_BUILD_TOOLS_VERSION=28.0.3 \
    ANDROID_SDK_PLATFORM_VERSION=28 \
    ANDROID_SDK_URL=https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip \
    ANDROID_HOME=/opt/android

ENV ANDROID_SDK_PACKAGES=" \
        tools \
        build-tools;$ANDROID_SDK_BUILD_TOOLS_VERSION \
        platform-tools \
        platforms;android-$ANDROID_SDK_PLATFORM_VERSION \
        extras;android;m2repository \
        extras;google;m2repository \
        extras;google;google_play_services \
    "

ENV PATH=$PATH:$ANDROID_HOME/tools/bin:$ANDROID_HOME/build-tools/$ANDROID_SDK_BUILD_TOOLS_VERSION:$ANDROID_HOME/platform-tools

RUN mkdir -p $ANDROID_HOME && \
    wget -q -O android.zip $ANDROID_SDK_URL && \
    unzip -q -d $ANDROID_HOME android.zip && \
    rm android.zip

RUN yes | sdkmanager --licenses && \
    yes | sdkmanager $ANDROID_SDK_PACKAGES
