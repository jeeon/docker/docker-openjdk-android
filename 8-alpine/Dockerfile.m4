#
# Use standard OpenJDK base image
#

FROM openjdk:8-alpine

include(common/maintainer.dockerfile)

include(common/install-gradle.dockerfile)

include(common/install-android.dockerfile)
