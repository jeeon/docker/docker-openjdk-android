# docker-android

Ready-made docker image for building and end-to-end testing of **Android Apps**.

Very specific versions (some non-latest) are used.

- openjdk
    - @ 8-stretch *(latest)*
    - @ 8-alpine
- gradle
    - @ 5.4.1
- android-sdk
    - tools @ 26.1.1
    - build-tools;28.0.3 @ 28.0.3
    - platform-tools @ 28.0.1
    - platforms;android-28 @ 6
    - extras;android;m2repository @ 47 *(prefer latest)*
    - extras;google;m2repository @ 58 *(prefer latest)*
    - extras;google;google_play_services @ 49 *(prefer latest)*

### Pull from Docker Hub
```
docker pull jeeon/android:8-default
docker pull jeeon/android:8-alpine
```

### Build from GitLab
```
docker build -t jeeon/android:8-default gitlab.com/jeeon/docker/android/8-default
docker build -t jeeon/android:8-alpine gitlab.com/jeeon/docker/android/8-alpine
```

### Run image
```
docker run -it jeeon/android:8-default bash
docker run -it jeeon/android:8-alpine bash
```

### Use as base image
```Dockerfile
FROM jeeon/android:8-default
FROM jeeon/android:8-alpine
```

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
